/*
1. Create a function which is able to prompt the user to provide their fullname, age, and location.
-use prompt() and store the returned value into a function scoped variable within the function.
-display the user's inputs in messages in the console.
-invoke the function to display your information in the console.
-follow the naming conventions for functions.
*/

//first function here:

/*
2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
-invoke the function to display your information in the console.
-follow the naming conventions for functions.

*/

//second function here:

/*
3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
-invoke the function to display your information in the console.
-follow the naming conventions for functions.

*/

//third function here:

/*
4. Debugging Practice - Debug the following codes and functions to avoid errors.
-check the variable names
-check the variable scope
-check function invocation/declaration
-comment out unusable codes.
*/
function whatisyourName(){
let userName = prompt('What is your Name:')
console.log(userName)

function whatisyourAge(){
let age = prompt('What is your Age:')
console.log(age)

function whatisyourCity(){
let city = prompt('What is your Location')
console.log(city)

alert('Thank you for you import')
}
whatisyourCity()
}
whatisyourAge()
}
whatisyourName()


function myBands(){
let band1 = ('1. The beatles')
console.log(band1)
let band2 =('2. Metallica')
console.log(band2)
let band3 = ('3. The Eagles')
console.log(band3)
let band4 = ('4. The Baklitas')
console.log(band4)
let band5 = ('5. The Eraserheads')
console.log(band5)
function myMovies(){
let movie1 = ('1. The Godfather')
console.log(movie1)
let rating1 = ('Rotten Tomatoes Rating: 97%')
console.log(rating1)
let movie2 =('2. The Godfather part II')
console.log(movie2)
let rating2 = ('Rotten Tomatoes Rating: 96%')
console.log(rating2)
let movie3 =('3. Peppa pig the final champter')
console.log(movie3)
let rating3 = ('Rotten Tomatoes Rating: 75%')
console.log(rating3)
let movie4 =('4. Doraemon the revenge')
console.log(movie4)
let rating4 = ('Rotten Tomatoes Rating: 66%')
console.log(rating4)
let movie5 =('5. the final season of 365days')
console.log(movie5)
let rating5 = ('Rotten Tomatoes Rating: 96%')
console.log(rating5)
}
myMovies()
}

myBands()



function printUsers(){
alert("Hi! Please add the names of your friends.");
let friend1 = prompt("Enter your first friend's name:");
let friend2 = prompt("Enter your second friend's name:");
let friend3 = prompt("Enter your third friend's name:");

console.log("You are friends with:")
console.log(friend1);
console.log(friend2);
console.log(friend3);
}

printUsers()